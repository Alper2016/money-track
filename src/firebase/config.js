import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyDGwVvj9FcnjT4nqpkGnMks91BGUFqNGrI",
    authDomain: "moneytrack-9ebf0.firebaseapp.com",
    projectId: "moneytrack-9ebf0",
    storageBucket: "moneytrack-9ebf0.appspot.com",
    messagingSenderId: "146497531919",
    appId: "1:146497531919:web:6be3d41dad762cab54737a"
  };

  //init firebase
  firebase.initializeApp(firebaseConfig)

  //init service
  const projectFirestore = firebase.firestore()
  const projectAuth = firebase.auth()

  // timestamp
  const timestamp = firebase.firestore.Timestamp

  export { projectFirestore, projectAuth, timestamp }